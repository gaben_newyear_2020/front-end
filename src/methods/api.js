import axios from 'axios';
import AsyncLocalStorage from '@createnextapp/async-local-storage'
import {device_pixel_ratio} from "javascript-retina-detect";

const headers = {
    'Content-type': 'application/json',
};

const instance = axios.create({
    method: 'post',
    headers,
    withCredentials: true,
});

const ApiUrl = 'https://postcard-newyear.gubanov.site/api/jsonrpc';
const AppLink = "https://vk.com/app7704043";
export const AppID = 7704043;

export async function startApp(sign) {
    return await instance({
        url: ApiUrl,
        data: {
            jsonrpc: '2.0',
            id: 0,
            method: 'user_open_app',
            params: {
                sign
            },
        },
    }).then((res) => (res.data.error ? null : res.data.result));
}
export async function users_get_info_in_vk(sign, user_ids) {
    return await instance({
        url: ApiUrl,
        data: {
            jsonrpc: '2.0',
            id: 0,
            method: 'users_get_info_in_vk',
            params: {
                sign,
                user_ids
            },
        },
    }).then((res) => (res.data.error ? null : res.data.result));
}
export async function postcardGetMy(sign) {
    return await instance({
        url: ApiUrl,
        data: {
            jsonrpc: '2.0',
            id: 0,
            method: 'postcard_my',
            params: {
                sign
            },
        },
    }).then((res) => (res.data.error ? null : res.data.result));
}
export async function postcard_random_greetings() {
    return await instance({
        url: ApiUrl,
        data: {
            jsonrpc: '2.0',
            id: 0,
            method: 'postcard_random_greetings',
            params: {},
        },
    }).then((res) => (res.data.error ? null : res.data.result));
}
export async function postcard_create(sign, design_id, text, to_vk_id = 0, anonymous= false, mark_id=0) {
    return await instance({
        url: ApiUrl,
        data: {
            jsonrpc: '2.0',
            id: 0,
            method: 'postcard_create',
            params: {
                sign,
                design_id,
                text,
                to_vk_id,
                anonymous,
                mark_id,
            },
        },
    }).then((res) => (res.data.error ? null : res.data.result));
}
export async function postcard_get(sign, slug) {
    return await instance({
        url: ApiUrl,
        data: {
            jsonrpc: '2.0',
            id: 0,
            method: 'postcard_get',
            params: {
                sign,
                slug,
            },
        },
    }).then((res) => (res.data.error ? null : res.data.result));
}

export async function postcard_create_link_by_slug(slug){
    return AppLink + "#link=postcard/" + slug;
}
export async function generateAdditionTextIntoPostcard(text){
    return await instance({
        url: ApiUrl,
        data: {
            jsonrpc: '2.0',
            id: 0,
            method: 'postcard_generate_greetings',
            params: {
                text,
            },
        },
    }).then((res) => (res.data.error ? null : res.data.result));
}
export async function postcard_validate_text(text){
    return await instance({
        url: ApiUrl,
        data: {
            jsonrpc: '2.0',
            id: 0,
            method: 'postcard_validate_text',
            params: {
                text,
            },
        },
    }).then((res) => (res.data.error ? null : res.data.result));
}
export function allPostcardShowItems(){
    const retina = device_pixel_ratio();
    const url = process.env.PUBLIC_URL;
    let preview_url = url + "/postcards/preview/";
    let source_url = url + "/postcards/";
    function set(id, source, color="#2A3A6C", preview=""){
        return {
            id: id,
            preview: preview_url + preview,
            source: source_url + source,
            color: color,
        }
    }
    function s(name){
        if (retina === 1){
            return name + ".png";
        }
        if ((retina > 1) && (retina < 3)){
            return name + "@2x.png";
        }
        if (retina >= 3){
            return name + "@3x.png";
        }
        return name + ".png";
    }

    return [
        set(1, s("1"), "#2A3A6C"),
        set(2, s("2"), "#B8DBD9"),
        set(3, s("3"), "#FF906E"),
        set(4, s("4"), "#3A5B8E")
    ]
}
export function getPostcardShowItemsByDesignId(uuid){
    let data = allPostcardShowItems();
    for (let i = 0; i < data.length; i++){
        if (data[i].id === uuid){
            return data[i];
        }
    }
    return null;
}
export function allPostcardMarkItems(){
    const url = process.env.PUBLIC_URL;
    const mark_url = url + "/postcards/marks/";
    function set(source, width, height, classItem=""){
        return {
            source: mark_url + source,
            width,
            height,
            classItem: classItem
        }
    }
    return [
        set('1.png', 270, 333),
        set('2.png', 479, 334),
        set('3.png', 479, 334),
        set('4.png', 270, 333),
        set('5.png', 270, 333),
        set('6.png', 270, 333),
        set('7.png', 309, 304),
        set('8.png', 309, 304),
        set('9.png', 310, 304),
    ]
}

export function validateEditText(text){
    if ((text === null) || (text.length === 0)){
        return "";
    }
    return text.replace(/(^\s+|\s+$)/g,'');
}

export function initCacheEditText(){
    const edit_text = validateEditText(localStorage.getItem("edit_text"));
    if ((edit_text === null) || (edit_text.length === 0)){
        localStorage.setItem("edit_text", "Поздравляю с Новым Годом!");
    }
    return localStorage.getItem("edit_text");
}

export function saveCacheEditText(text){
    const text_valid = validateEditText(text);
    if (text_valid.length > 0){
        localStorage.setItem("edit_text", text_valid);
    }
}

export function initCachePostcardIndex(){
    let edit_text = localStorage.getItem("postcard_index_prev");
    if ((edit_text === null) || (edit_text.length === 0) || isNaN(parseInt(edit_text))){
        localStorage.setItem("postcard_index_prev", "0");
        edit_text = "0";
    }

    let index = parseInt(edit_text);
    const dataPostcards = allPostcardShowItems();
    if ((index >= dataPostcards.length) && (dataPostcards.length > 0)){
        saveCachePostcardIndex(0);
        index = 0;
    }

    return index;
}

export function initCachePostcardIndexAndGetDesignId(){
    let index = initCachePostcardIndex();
    return allPostcardShowItems()[index].id;
}

function saveCachePostcardIndex(index){
    localStorage.setItem("postcard_index_prev", index);
}

export async function asyncSaveCachePostcardIndex(index){
    try{
        await AsyncLocalStorage.setItem("postcard_index_prev", index);
    } catch(e) {}
}

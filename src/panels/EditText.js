import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import {Panel} from '@vkontakte/vkui';
import {PrevArrowIcon, PanelHeaderUi, ButtonUiSecondary, ButtonUiPrimary, TabbarUi} from "./compacts/components";
import * as api from "../methods/api";
import TextareaAutosize from 'react-autosize-textarea';

function link_to_click() {
	try{
		document.querySelector("#link-to-main-screen-from-edit").click();
	} catch (e) {}
}

const get_count_words = (string) => {
	let ans = 0;
	if ((string === undefined) || (string === null) || (string === "")){
		return ans;
	}
	string = string.replace(/[\r\n]+/g, ' ');
	for (const a of string.split(' ')) {
		if (a.length !== 0){
			ans++;
		}
	}
	return ans;
}

const EditTextScreen = ({ id, fetchedUser }) => {
	const [text, setText] = useState(api.initCacheEditText());
	const [additionTextOne, setAdditionTextOne] = useState("");
	const [additionTextIndexState, setAdditionTextIndexState] = useState(0);
	const [counterWord, setCounterWord] = useState(get_count_words(text));
	const [additionText, setAdditionText] = useState([]);

	async function generateAdditionText(){
		let data = await api.generateAdditionTextIntoPostcard(text);
		try{
			data = data.replies;
			setAdditionText(data);
			setAdditionTextIndexState(0);
			setAdditionTextOne(data[0]);
		} catch (e) {}
	}
	function addTextAndThenEdit(){
		setText(text + additionTextOne);
		setAdditionTextOne("");
		setAdditionText([]);
	}
	function nextExampleText(){
		if ((additionTextIndexState + 1) >= (additionText.length)){
			generateAdditionText(text);
		} else {
			setAdditionTextIndexState(additionTextIndexState + 1);
			setAdditionTextOne(additionText[additionTextIndexState + 1]);
		}
	}

	useEffect(() => {
		async function save(){
			api.saveCacheEditText(text);
			setCounterWord(get_count_words(text));
		}
		save();
	}, [text]);

	const ButtonUiSecondaryElement = (additionText.length === 0) ? (
		<ButtonUiSecondary onClick={() => {generateAdditionText()}} >Дополнить текст нейросетью</ButtonUiSecondary>
	) : (
		<ButtonUiSecondary onClick={nextExampleText} addIcon={true}>Ещё варианты</ButtonUiSecondary>
	)
	
	return (
		<Panel id={id}>
			<div>
				<PanelHeaderUi icon={
					(<Link to={"/"}><PrevArrowIcon /></Link>)
				}>Редактор</PanelHeaderUi>
				<div className={"DivElementMargin__default editable__textarea"}>
					<div className={"Textarea"}>
						{(additionText.length === 0) &&
							<TextareaAutosize style={{ minHeight: 100, maxHeight: 250 }} onChange={(e) => {
								setText(e.target.value);
							}} defaultValue={text} value={text}/>
						}
						{(additionText.length !== 0) &&
							<div onClick={addTextAndThenEdit} style={{ minHeight: 100, maxHeight: 250 }} className={"textarea"}>
								{text}<span className="add">{additionTextOne}</span>
							</div>
						}
						<div className="counter-word">{counterWord}/30</div>
					</div>
					{ButtonUiSecondaryElement}
				</div>
			</div>
			<TabbarUi>
				<div onClick={() => {
					addTextAndThenEdit();
					setTimeout(() => {
						link_to_click();
					}, 100);
				}}>
					<Link id="link-to-main-screen-from-edit" to="/" />
					<ButtonUiPrimary style={{width: document.body.clientWidth - 32}}>Готово</ButtonUiPrimary>
				</div>
			</TabbarUi>
		</Panel>
	);
}


export default EditTextScreen;

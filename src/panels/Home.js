import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import PanelHeader from '@vkontakte/vkui/dist/components/PanelHeader/PanelHeader';
import Button from '@vkontakte/vkui/dist/components/Button/Button';
import Group from '@vkontakte/vkui/dist/components/Group/Group';
import Cell from '@vkontakte/vkui/dist/components/Cell/Cell';
import Div from '@vkontakte/vkui/dist/components/Div/Div';
import Avatar from '@vkontakte/vkui/dist/components/Avatar/Avatar';
import View from "@vkontakte/vkui/dist/components/View/View";
import bridge from "@vkontakte/vk-bridge";

async function get_friends(){
	const all_friends = await bridge.send("VKWebAppGetFriends", {});
}

const Home = ({ id, fetchedUser }) => (
	<Panel id={id}>
		<PanelHeader>Example</PanelHeader>
		{fetchedUser &&
		<Group title="User Data Fetched with VK Bridge">
			<Cell
				before={fetchedUser.photo_200 ? <Avatar src={fetchedUser.photo_200}/> : null}
				description={fetchedUser.city && fetchedUser.city.title ? fetchedUser.city.title : ''}
			>
				{`${fetchedUser.first_name} ${fetchedUser.last_name}`}
			</Cell>
		</Group>}

		<Group title="Navigation Example">
			<Div>
				<Link to="/test/" style={{ textDecoration: 'none' }}>
					<Button size="xl" level="2" data-to="persik">
						Show me the Persik, please
					</Button>
				</Link>
			</Div>
			<Div>
				<Button size="xl" level="3"  id='btn-friends' onClick={() => {get_friends()}} >Получить список друзей пользователя</Button>
			</Div>
			<Div>
				<Link to="/postcard/" style={{ textDecoration: 'none' }}>
					<Button size="xl" level="2">
						Перейти к почтовому ящику
					</Button>
				</Link>
			</Div>
		</Group>
	</Panel>
);

Home.propTypes = {
	id: PropTypes.string.isRequired,
	fetchedUser: PropTypes.shape({
		photo_200: PropTypes.string,
		first_name: PropTypes.string,
		last_name: PropTypes.string,
		city: PropTypes.shape({
			title: PropTypes.string,
		}),
	}),
};

export default Home;

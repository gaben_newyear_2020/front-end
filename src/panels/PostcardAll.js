import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
import {Spinner, Panel, SimpleCell, Avatar} from '@vkontakte/vkui';
import * as api from "../methods/api";
import Snowman from "./compacts/snowman";
import {offset, PanelHeaderUi, PrevArrowIcon} from "./compacts/components";
import useWindowDimensions from "../methods/hooks/useWindowDimensions";

function getBeautifyDateAndTime(time){
    function simple_info(now_time, old_time){
        return (
            (now_time.getFullYear() === old_time.getFullYear()) &&
            (now_time.getMonth() === old_time.getMonth()) &&
            (now_time.getDate() === old_time.getDate())
        )
    }
    function isValidDate(d) {
        return d instanceof Date && !isNaN(d);
    }
    function output_time(old_time){
        let h = old_time.getHours() + "";
        let m = old_time.getMinutes() + "";
        if (h.length === 1){
            h = "0" + h;
        }
        if (m.length === 1){
            m = "0" + m;
        }
        return h + ":" + m;
    }

    function formatDate(date) {

        let dd = date.getDate();
        if (dd < 10) dd = '0' + dd;

        let mm = date.getMonth();
        const month = ["янв", "фев", "мар", "апр", "май", "июн", "июл", "авг", "сен", "окт", "ноя", "дек"];
        mm = month[mm];

        let yy = date.getFullYear() % 100;
        if (yy < 10) yy = '0' + yy;

        return dd + ' ' + mm;
    }

    let datetime = new Date(time + "Z");
    let now = new Date();
    if (!isValidDate(datetime)){
        return "";
    }
    if (simple_info(now, datetime)){
        // сегодня
        return "сегодня " + output_time(datetime);
    }
    now.setDate(now.getDate() - 1);
    if (simple_info(now, datetime)){
        // вчера
        return "вчера " + output_time(datetime);
    }
    return output_time(datetime) + ", " + formatDate(datetime);
}

const PostcardRawItem = ({data}) => {
    let user_default = {
        first_name: "Аноним",
        id: 0,
        last_name: "",
        can_access_closed: true,
        is_closed: false,
        photo_200: "https://ic.pics.livejournal.com/chestniy_yurist/47888261/57075/57075_original.png",
        time_send: null
    }
    if (data.from_user !== null){
        user_default = data.from_user
    }
    let after_icon = data.is_view ? (<span/>) : (<span style={{color: "#0073ff", fontSize: "34px"}}>&bull;</span>);
    return (
        <div>
            <Link to={"/postcard/" + data.slug}>
                <SimpleCell
                    before={<Avatar size={40} src={user_default.photo_200} />}
                    after={after_icon}
                    description={
                        (data.time_send !== undefined) ? getBeautifyDateAndTime(data.time_send) : ""
                    }
                >{user_default.first_name + " " + user_default.last_name}
                </SimpleCell>
            </Link>
        </div>
    )
}
const PostcardRaw = ({data}) => {
    const document_body = useWindowDimensions();
    const [maxHeight, setMaxHeight] = useState("100vh");
    useEffect(() => {
        try{
            const height = offset(document.querySelector('.ViewPostcardAll')).top;
            setMaxHeight(document_body.height - height);
        } catch (e) {
            setMaxHeight("100vh");
        }
    }, [document_body]);

    if (data.length > 0){
        let view = data.map((item, key) => {
            return <PostcardRawItem data={item} key={key}/>
        });
        return (
            <div className={"ViewPostcardAll"} style={{overflow: "auto", maxHeight: maxHeight}}>
                {view}
            </div>
        )
    }
    return (
        <div className={"Empty"}>
            <div className="Empty__icon">
                <Snowman/>
            </div>
            <div className="Empty__text">
                Писем ещё нет
            </div>
        </div>
    )
}

const PostcardAll = ({id, fetchedUser, appUser}) => {

    const [data, setData] = useState([]);
    const [loader, setLoader] = useState(true);
    useEffect(() => {
        async function fetchedData(){
            let data = await api.postcardGetMy(appUser.sign);
            if ((data !== undefined) && (data.postcards !== undefined) && (data.count > 0)){
                setData(data.postcards);
            }
            setLoader(false);
        }
        fetchedData();
    }, []);
    let view = (<div/>);
    if (loader){
        view = (
            <div style={{ display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
                <Spinner size="medium" style={{ marginTop: 20 }} />
            </div>
        )
    } else{
        view = (
            <PostcardRaw data={data}/>
        )
    }
    return (
        <Panel id={id}>
            <PanelHeaderUi only_ui={true} icon={
                (<Link to={"/"}><PrevArrowIcon /></Link>)
            }>Уведомления</PanelHeaderUi>
            {view}
        </Panel>
    )
};

export default PostcardAll;

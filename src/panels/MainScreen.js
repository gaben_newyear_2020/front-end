import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import Panel from '@vkontakte/vkui/dist/components/Panel/Panel';
import {ButtonUi, MailIconAndCounter, offset, PanelHeaderUi, TabbarUi} from "./compacts/components";
import {Gallery, Spinner} from "@vkontakte/vkui";
import * as api from "../methods/api";
import editpen from "./compacts/icons/editpen.svg"
import useWindowDimensions from "../methods/hooks/useWindowDimensions";



const IconPen = () => {
	return (
		<div className="edit">
			<Link  to={"/edit/"}>
				<div style={{width: "100%", height: "100%"}}>
					<div className={"absolute-center"}>
						<img src={editpen} alt="editPen"/>
					</div>
				</div>
			</Link>
		</div>
	)
}


const PostcardMenuItem = ({size, active, edit_text, item, SizePostcardMenuItem}) => {
	// View Main Screen PostcardMenuItem
	const [isLoaded, setIsLoaded] = useState(false);
	const style = {
		width: SizePostcardMenuItem.width,
		height: SizePostcardMenuItem.height,
		backgroundColor: item.color,
		backgroundImage: isLoaded ? "url(" + item.source + ")": null,
		backgroundPosition: "right bottom",
		backgroundSize: "cover",
		backgroundRepeat: "no-repeat"
	}
	const width_text = SizePostcardMenuItem.width_text;
	const [activeLocal, setActiveLocal] = useState(false);

	useEffect(() => {
		setTimeout(() => {
			setActiveLocal(active);
		}, 300);
	}, [active]);

	useEffect(() => {
		const imageLoader = new Image();
		imageLoader.src = item.source;
		imageLoader.onload = () => {
			setIsLoaded(true);
		};
	}, []);

	return (
		<div className={"gallery__container"}>
			<div style={style} className={"postcard-menu-item " +  (activeLocal ? "active " : "")}>
				<div style={{width: width_text, visibility: (activeLocal ? "visible" : "hidden")}}>
					<IconPen />
					<div style={{width: width_text}} className="text-block">
						<div>{edit_text}</div>
					</div>
				</div>
				{!isLoaded &&
					<Spinner size="large" style={{margin:"auto",top: "50%",left: "50%",transform: "translate(-50%, -50%)",position: "absolute"}} />
				}
			</div>
		</div>
	);
}
const DotElementItem = ({isActive=false}) => {
	const [activeLocal, setActiveLocal] = useState(false);
	useEffect(() => {
		setTimeout(() => {
			setActiveLocal(isActive);
		}, 380);
	}, [isActive]);

	return (
		<div className={"dot " + (activeLocal ? "active ": "")}/>
	)
}
const PostcardMenu = ({heightGallery, dataPostcards = []}) => {
	// heightGallery параметр доступной высоты, но он будет меньше
	const [slideIndex, setSlideIndex] = useState(0);
	const document_body = useWindowDimensions();
	const width = document_body.width - 24 - 24;
	const size = {height: heightGallery.height, width: width};

	const getSizesBySizes = (width, height) => {
		let resp = {};
		if (width < height){
			 resp = {
				width: width - 16,
				height: Math.round((((400 / 280) * width) + Number.EPSILON) * 100) / 100,
				width_text: width - 16 - 24,
			}
		}
		if ((width >= height) || (resp.height > height)){
			const tmp = height - 16;
			const tmp2 = Math.round((((280 / 400) * tmp) + Number.EPSILON) * 100) / 100;
			resp = {
				width: tmp2,
				height: tmp,
				width_text: tmp2 - 24,
			}
		}
		return resp;
	}
	const SizePostcardMenuItem = getSizesBySizes(width, heightGallery.height);


	const [edit_text, setEditText] = useState("");

	let PostcardMenuItems = dataPostcards.map((item, index) => {
		return (
			<PostcardMenuItem edit_text={edit_text} SizePostcardMenuItem={SizePostcardMenuItem} item={item} size={size} key={index} active={index === slideIndex}/>
		)
	});
	let PostcardMenuDotsElements = dataPostcards.map((item, index) => {
		return (
			<DotElementItem key={index} isActive={index === slideIndex}/>
		)
	});
	useEffect(() => {
		async function load(){
			let index = api.initCachePostcardIndex();
			setSlideIndex(index);
			setEditText(api.initCacheEditText());
		}
		load();
	}, [dataPostcards]);

	return (
		<div className="postcard-menu">
			{(dataPostcards.length > 0) &&
				<div style={{
					position: "relative",
					width: "100%",
					height: heightGallery.height,
					paddingTop: (heightGallery.height - (SizePostcardMenuItem.height  + 20)) / 2
				}}>
						<Gallery
							slideWidth={width + "px"}
							align="center"
							style={{ height: SizePostcardMenuItem.height  + 20}}
							slideIndex={slideIndex}
							onChange={async (slideIndex) => {
								setSlideIndex(slideIndex);
								await api.asyncSaveCachePostcardIndex(slideIndex);
							}}
						>
							{PostcardMenuItems}
						</Gallery>
						<div className="elements__dots">
							{PostcardMenuDotsElements}
						</div>
				</div>

			}
		</div>
	)
}
const MainScreen = ({ id, appUser, MainScreenCounterUnread=null}) => {
	const document_body = useWindowDimensions();
	const [heightHeader, setHeightHeader] = useState({height: 30});
	const [heightGallery, setHeightGallery] = useState({
		height: document_body.height - heightHeader.height - 107,
	});
	const [dataPostcards, setDataPostcards] = useState([]);
	useEffect(() => {
		async function load(){
			const height = offset(document.querySelector('.postcard-menu')).top
			setHeightHeader({height});
			setDataPostcards(api.allPostcardShowItems());
		}
		load();
	}, [document_body]);

	useEffect(() => {
		setHeightGallery({
			height: document_body.height - heightHeader.height - 107,
		});
	}, [heightHeader]);
	return (
		<Panel id={id}>
			<div style={{position: "relative", height: "100vh", overflow: "hidden"}} className={"blue_background_gradient"}>
				<PanelHeaderUi id={"headerFirstScreen"} icon={
					(<Link to={"/postcard/"}><MailIconAndCounter counter={
						((MainScreenCounterUnread !== null) &&
						(MainScreenCounterUnread !== undefined))? MainScreenCounterUnread: 0
					}/></Link>)
				}>Открыточки</PanelHeaderUi>
				<PostcardMenu heightGallery={heightGallery} dataPostcards={dataPostcards}/>
				<TabbarUi>
					<div className={"button-down"}>
						<Link to={"/postcard-new/"}>
							<ButtonUi>Отправить открытку</ButtonUi>
						</Link>
					</div>
				</TabbarUi>
			</div>
		</Panel>
	);
}


export default MainScreen;

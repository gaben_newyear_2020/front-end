import React, {useEffect, useState} from 'react';
import { FormLayout, Spinner, Gallery, Button, Div, Group, Headline, Textarea, Panel, PanelHeader, PanelHeaderBack} from '@vkontakte/vkui';
import * as api from "../../methods/api";

const PatterStart = "C Новым Годом!\n";
const PatterTemplateStart = ", поздравляю тебя с Новым Годом!\n";

function generateStartText(object){
    if (object.to_vk_object !== null){
        return object.to_vk_object.first_name + PatterTemplateStart;
    }
    return "";
}

const NextCardItem = ({text, new_text, handler}) => {
    if ((text.length > 0) && (text[text.length - 1] !== " ") && (new_text.length > 0) && (new_text[0] !== " ")){
        text += " ";
    }
    return (
    <div onClick={() => {
        handler(text, new_text);
    }} className="card-main">
        <FormLayout>
            <Div className="card-item">
                <p>
                    <span>{text}</span>
                    <span className="new">{new_text}</span>
                </p>
            </Div>
        </FormLayout>
    </div>

    )
}

const ButtonNext = ({text, handler}) => {
    const [load, setLoad] = useState(false);
    let allow_next = text.replace(/(^\s+|\s+$)/g,'').length > 0;
    let btn = "";
    if (allow_next && !load){
        btn = (
            <Button onClick={() => {
                setLoad(true);
                handler();
            }} stretched size="l">
                Дальше
            </Button>
        )
    } else {
        btn = (
            <Button stretched disabled="disabled" size="l">
                {load ? (<Spinner size="small" />) : "Дальше"}
            </Button>
        )
    }
    return (
        <Div style={{transition: ".3s"}}>
            {btn}
        </Div>
    )
}
const StepTwo = ({id, changePanel, prev, next, params}) => {
    const [work, setWork] = useState(true);
    const [text, setText] = useState(generateStartText(params[0]));
    const [slideIndex, setSlideIndex] = useState(0);
    const [additionText, setAdditionText] = useState([]);

    async function generateAdditionText(new_value){
        let data = await api.generateAdditionTextIntoPostcard(new_value);
        try{
            data = data.replies;
            if (work){
                setAdditionText(data);
            }
        } catch (e) {}
    }

    useEffect(() => {
        async function fetchData(){
            let data = await api.postcard_random_greetings();
            let data_set = [
                "Желаю исполнения новых желаний!"
            ]
            try{
                data_set = data.greetings.map((elem) => {
                    return elem.text;
                })
            } catch (e) {}
            if (work){
                setAdditionText(data_set);
            }
        };
        fetchData();
        return () => {
            setWork(false);
        }
    }, [])

    async function handler(text, new_text){
        setSlideIndex(0);
        setText(text + new_text);
        const await1 = await generateAdditionText(text + new_text);
        await await1;
    }
    async function next_step(){
        let params_new = params[0];
        params_new.text = text.replace(/(^\s+|\s+$)/g,'').replace(/ {1,}/g," ");
        if (params[0].to_vk_object !== null){
            let data = await api.postcard_validate_text(params_new.text);
            if ((data !== null) && (data.is_valid === true)){
                params_new.anonymous_access = true;
            }
        }
        params[1](params_new);
        changePanel(next);
    }
    let additionView = additionText.map(
        (elem, i) => {return <NextCardItem key={i} text={text} handler={handler} new_text={elem}/>}
    )
    return (
        <Panel id={id}>
            <PanelHeader separator={false} left={
                <div onClick={() => {changePanel(prev)}}>
                    <PanelHeaderBack/>
                </div>
            }>
                Новое сообщение
            </PanelHeader>
            <Group header={
                <div className="UpInputGroupName">
                    <Headline weight="regular">Твоё поздравление:</Headline>
                </div>
                }
            >
                <Gallery
                    slideWidth="100%"
                    align="center"
                    style={{ height: "auto" }}
                    slideIndex={slideIndex}
                    onChange={(slideIndex) => {
                        setSlideIndex(slideIndex);
                    }}
                >
                    <div id="postcard__step-two--area">
                        <FormLayout>
                            <Textarea onChange={(e) => {
                                setText(e.target.value);
                                generateAdditionText(e.target.value);
                            }} placeholder={PatterStart} defaultValue={text} value={text}/>
                        </FormLayout>
                    </div>
                    {additionView}
                </Gallery>
            </Group>
            <ButtonNext text={text} handler={next_step}/>

        </Panel>
    )
};

export default StepTwo;

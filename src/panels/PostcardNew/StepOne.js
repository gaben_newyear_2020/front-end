import React, {useEffect} from 'react';
import {Panel} from '@vkontakte/vkui';

import bridge from "@vkontakte/vk-bridge";


const StepOne = ({id, changePanel, prev, next, params}) => {
    useEffect(() => {
        async function g(){
            try{
                const friend = await bridge.send("VKWebAppGetFriends", {});
                let params_new = params[0];
                params_new.to_vk_id = friend.users[0].id;
                params_new.to_vk_object = friend.users[0];
                params[1](params_new);
                changePanel(next);

            } catch (e) {
                if (e.error_data.error_code === 6) {
                    changePanel(next);
                } else {
                    changePanel(prev);
                }
            }
        }
        g();
        
    }, [])
    return (
        <Panel id={id}></Panel>
    )
};

export default StepOne;

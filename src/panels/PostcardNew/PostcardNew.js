import React, {useState} from 'react';
import { View} from '@vkontakte/vkui';
import {allPostcardMarkItems} from "../../methods/api";
import {randomInteger} from "../../methods/utils";

import StepOne from "./StepOne";
import StepZero from "./StepZero";
import StepThree from "./StepThree";
import StepFinal from "./StepFinal";
import * as api from "../../methods/api";

const RandomMark = () => {
    const data = allPostcardMarkItems();
    const id = randomInteger(0, data.length - 1);
    let data_mark = data[id];
    data_mark.id = id;
    return data_mark;
}

const PostcardNew = ({id, fetchedUser, appUser, back}) => {
    const data = useState({
        design_id: api.initCachePostcardIndexAndGetDesignId(),
        text: api.initCacheEditText(),
        to_vk_id: null,
        to_vk_object: null,
        anonymous_access: false,
        anonymous: false,
        mark: RandomMark(),
    });

    const [activePanel, setActivePanel] = useState("step-one");

    return (
        <View id={id} activePanel={activePanel}>
            <StepZero back={back} id="step-zero"/>

            <StepOne params={data} changePanel={setActivePanel}
                     id="step-one" next="step-three" prev="step-zero"/>
            <StepThree UserFetch={fetchedUser} params={data} changePanel={setActivePanel}
                     id="step-three" next="step-final" prev="step-zero"/>
            <StepFinal appUser={appUser} params={data} changePanel={setActivePanel}
                       id="step-final" next="step-zero" prev="step-zero"/>
        </View>
    )
};

export default PostcardNew;

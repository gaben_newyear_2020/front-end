import React, {useEffect, useState} from 'react';
import { v4 as uuidv4 } from 'uuid';
import {Button, Placeholder, Spinner, Panel, PanelHeader, PanelHeaderBack} from '@vkontakte/vkui';
import Icon56CheckCircleOutline from '@vkontakte/icons/dist/56/check_circle_outline';
import Icon56ErrorOutline from '@vkontakte/icons/dist/56/error_outline';
import * as api from "../../methods/api";
import bridge from "@vkontakte/vk-bridge";
import {randomInteger} from "../../methods/utils";
import {PanelHeaderUi, PrevArrowIcon} from "../compacts/components";

function generateTextMessage(slug){
    return "Поздравляю тебя с Новогодними праздниками!\n" + slug
}
function generateTextSendVkMessage(first_name){
    return "Привет, " + first_name + "."
}

async function SendMessageInVk(slug, userFriend){
    return false;
    try{
        const data = await bridge.send("VKWebAppGetAuthToken", {
            "app_id": api.AppID, "scope": "messages"
        });
        const token = data.access_token;
        const request_id = uuidv4();
        const random_id = randomInteger(0, Math.pow(10, 9));
        const message = generateTextSendVkMessage(userFriend.first_name) + " " + generateTextMessage(slug);
        const send_data = await bridge.send("VKWebAppCallAPIMethod", {
            "method": "messages.send", "request_id": request_id,
            "params": {
                "user_id": userFriend.id,
                "random_id": random_id,
                "message": message,
                "v":"5.124",
                "access_token":token
            }
        });
        // Todo: Изменить поведение в функции запроса messages.send
        // console.log(send_data);
        return !send_data.hasOwnProperty("error");
    } catch (e) {
        // console.log("ERR", e);
        return false;
    }

}

async function StackInShareWindow(){
    let data = await bridge.send("VKWebAppGetClientVersion");
    return !(data.platform === "web")
}

async function ShareWindow(slug){
    try {
        await bridge.send("VKWebAppShare", {"link": slug});
        return true;
    } catch (e) {
        return false;
    }
}

async function ShareCopy(slug){
    try {
        await bridge.send("VKWebAppCopyText", {"text": generateTextMessage(slug)});
        return true;
    } catch (e) {
        return false;
    }
}


const PlaceholderPage = ({status, onError, onOk, textMyError=""}) => {
    if (status === "ok"){
        return (
            <Placeholder
                icon={<Icon56CheckCircleOutline />}
                action={<Button onClick={onOk} size="l" mode="tertiary">Отправить ещё</Button>}
                stretched
            >
                Сообщение успешно отправлено
            </Placeholder>
        )
    } else if (status === "error") {
        return (
            <Placeholder
                icon={<Icon56ErrorOutline />}
                action={<Button onClick={onError} size="l" mode="tertiary">Попробовать ещё раз</Button>}
                stretched
            >
                Сообщение не отправлено
            </Placeholder>
        )
    } else if (status === "copy") {
        return (
            <Placeholder
                icon={<Icon56CheckCircleOutline />}
                action={
                    <a href="https://vk.com/im" target="_blank">
                        <Button size="l" mode="tertiary">Перейти в диалоги</Button>
                    </a>
                }
                stretched
            >
                Ссылка скопирована в буфер обмена
            </Placeholder>
        )
    } else if (status === "myError") {
        return (
            <Placeholder
                icon={<Icon56ErrorOutline />}
                action={<Button onClick={onOk} size="l" mode="tertiary">К выбору открытки</Button>}
                stretched
            >
                {textMyError}
            </Placeholder>
        )
    } else {
        return (
            <div style={{ display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
                <Spinner size="large" style={{ marginTop: 20 }} />
            </div>
        )
    }
}
const StepFinal = ({id, changePanel, prev, next, params, appUser}) => {
    const UserFromData = params[0];
    const [status, setStatus] = useState("");
    const [textMyError, setTextMyError] = useState("");
    const [link, setLink] = useState("");

    async function fetchData(){
        const userFetched = UserFromData.to_vk_id !== null;
        const isAnon = (UserFromData.to_vk_id !== null) && (UserFromData.anonymous_access) && (UserFromData.anonymous);

        let slug_data = link;
        if (slug_data === "") {
            let data = await api.postcard_create(
                appUser.sign,
                UserFromData.design_id,
                UserFromData.text,
                userFetched ? UserFromData.to_vk_id : 0,
                isAnon,
                UserFromData.mark.id,
            );
            if ((data === null) || (data.postcard_slug === undefined)) {
                if (data.error === undefined) {
                    setStatus("error");
                } else {
                    setTextMyError(data.error);
                    setStatus("myError");
                }
                return null;
            }
            slug_data = await api.postcard_create_link_by_slug(data.postcard_slug);
            setLink(slug_data);
        }
        if (isAnon){
            setStatus("ok");
            return null;
        }
        let token_success = false;
        if (userFetched){
            token_success = await SendMessageInVk(slug_data, UserFromData.to_vk_object);
        }
        if (token_success){
            setStatus("ok");
            return null;
        }
        if (await StackInShareWindow()){
            // Поделиться экраном, если только не WEB версия
            let status = await ShareWindow(slug_data);
            setStatus(status ? "ok" : "error");
            return null;
        }
        let status = await ShareCopy(slug_data);
        setStatus(status ? "copy" : "error");
    }

    useEffect(() => {
        fetchData();
    }, [])
    return (
        <Panel id={id}>
            <PanelHeaderUi icon={
                (<div onClick={() => {
                    if (status !== ""){
                        changePanel(prev);
                    }
                }}><PrevArrowIcon /></div>)
            }>Отправка</PanelHeaderUi>

            <PlaceholderPage status={status} onOk={() => {
                changePanel(next);
            }} onError={() => {
                setStatus("");
                fetchData();
            }} textMyError={textMyError}
            />
        </Panel>
    )
};

export default StepFinal;

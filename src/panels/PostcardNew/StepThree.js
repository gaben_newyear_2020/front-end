import React, {useEffect, useState} from 'react';
import {Panel, Tabbar} from '@vkontakte/vkui';
import * as api from "../../methods/api";
import {ButtonUi, Envelope, PanelHeaderUi, PrevArrowIcon} from "../compacts/components";

const StepThree = ({id, changePanel, prev, next, params, UserFetch}) => {
    const [view, setView] = useState(false);
    useEffect(() => {
        setTimeout(() => {
            if (view === false){
                setView(true);
            }
        }, 450);
    }, []);

    return (
        <Panel id={id}>
            <div style={{
                position: "relative",
                height: "100vh"
            }}>
                <PanelHeaderUi icon={
                    (<div onClick={() => {
                        changePanel(prev);
                    }}><PrevArrowIcon /></div>)
                    }>Отправка</PanelHeaderUi>
                <div hidden={!view} className={"absolute-center"}>
                    <div className={"postcard-three-panel"}>
                        <Envelope is_active={false} from_user={UserFetch} to_user={params[0].to_vk_object} mark={params[0].mark}
                                  postcard_text={params[0].text}
                                  design_item={api.getPostcardShowItemsByDesignId(params[0].design_id)}
                        />
                    </div>
                </div>
                <Tabbar shadow={false}>
                    <div style={{
                        padding: "0 30px 16px 30px !important",
                        width: "100%"
                    }} onClick={() => {
                        changePanel(next);
                    }}>
                        <ButtonUi>Отправить</ButtonUi>
                    </div>
                </Tabbar>
            </div>
        </Panel>
    )
};

export default StepThree;

import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
import { Panel } from '@vkontakte/vkui';

function link_to_click(id) {
    document.querySelector("#"+id).click();
}

const StepZero = ({id, back}) => {
    useEffect(() => {
        link_to_click(id + "__link");

    }, []);
    return (
        <Panel id={id}>
            <Link id={id + "__link"} to={back}/>
        </Panel>
    )
};

export default StepZero;

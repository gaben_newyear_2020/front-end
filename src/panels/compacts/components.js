import mail from '../compacts/icons/mail.svg';
import React, {useEffect, useState} from 'react';
import sendto from '../compacts/icons/sendto.png'
import useWindowDimensions from "../../methods/hooks/useWindowDimensions";
import topEnvelope from "./icons/topEnvelope.svg";
import indexDigitPostcard from "./icons/indexDigitPostcard.png";
import {Avatar, Div, PanelHeader, Tabbar} from '@vkontakte/vkui';
import Icon28ArrowLeftOutline from '@vkontakte/icons/dist/28/arrow_left_outline';
import Icon28RefreshOutline from '@vkontakte/icons/dist/28/refresh_outline';

export const TabbarUi = ({children}) => {
    return (
            <Tabbar shadow={false}>
                <div style={{
                    position: "relative",
                    width: "100%",
                    height: "47px",
                    paddingBottom: "16px"
                }}>
                    {children}
                </div>
            </Tabbar>
        )
}
export function offset(el) {
    const rect = el.getBoundingClientRect(),
        scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
        scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
}

export const PanelHeaderUi = ({children, icon, id}) => {
    return (
        <PanelHeader separator={false} id={id} className={"header-screen"} left={
            <Div>
                <header>
                    <div className="nav_icon">
                        <div className="nav_icon_item">
                            {icon}
                        </div>
                    </div>
                    <span>
                        {children}
                    </span>
                </header>
            </Div>
        }> </PanelHeader>
    )
}

export const MailIconAndCounter = ({counter = 0}) => {
    return (
        <div className="mailIcon anyheadericon">
            <img src={mail} alt="mailIcon"/>
            { (counter > 0) && (counter !== undefined) && (counter !== null) &&
            <div className="counter">
                {counter}
            </div>
            }
        </div>
    )
}

export const PrevArrowIcon = () => {
    return (
        <div className="anyheadericon prev">
            <Icon28ArrowLeftOutline width={32} height={32}/>
        </div>
    )
}

export const ButtonUi = ({children, addIcon=true}) => {
    return <ButtonUiRender className={"button_blue_ui"} children={children} addIcon={addIcon}/>
}
export const ButtonUiOrange = ({children, addIcon=true}) => {
    return <ButtonUiRender className={"button_blue_ui orange"} addIcon={addIcon} children={children}/>
}
const ButtonUiRender = ({children, addIcon=true, className}) => {
    return (
        <div className={className}>
            <button>
                <span className="text">
                     {children}
                    { addIcon &&
                    <img style={{width: "24px", height:"24px"}} className="text-icon" src={sendto} alt="SendToIcon"/>
                    }
                </span>
            </button>
        </div>
    );
}

export const ButtonUiSecondary = ({children, onClick, addIcon=false}) => {
    return (
        <div className={"button_blue_ui_secondary"}>
            <button onClick={onClick}>
                { addIcon &&
                    <div className={"text-icon"}>
                        <Icon28RefreshOutline width={20} height={20}/>
                    </div>
                }
                <span className="text">
                     {children}
                </span>
            </button>
        </div>
    );
}

export const ButtonUiPrimary = ({children, onClick, style}) => {
    return (
        <div style={style} className={"button_blue_ui_primary"}>
            <button onClick={onClick}>
                <span className="text">
                     {children}
                </span>
            </button>
        </div>
    );
}

const EnvelopeElementWriteIndex = () => {
    let elements = [0, 0, 0, 0, 0, 0].map((item, index) => {
        return (
            <img src={indexDigitPostcard} alt={"index|" + index} key={index}/>
        )
    })
    return (
        <div className={"envelope__base__index"}>
            {elements}
        </div>
    )
}
const NameInEnvelopeAndAvatar = ({user, add_margin=false, text="Кому"}) =>{
    return (
        <div className={"block_names_avatar"}>
            <div className="second_text">{text}</div>
            <div className="flex-container title_block">
                <div className={"flex-item avatar"}>
                    <Avatar size={40} src={user.photo_200} />
                </div>
                <div className="flex-item title_text">{user.first_name + " " + user.last_name}</div>
            </div>
        </div>
    )
}
function PostcardMenuItemView_dim(d_body){
    let alternative = false;
    let width = 0;
    let height = 0;
    if (d_body.width < d_body.height){
        width = d_body.width - 24 - 24 - 8;
        height = (448/312)*width;
        alternative = (height > (d_body.height - 180));
    }
    if ((d_body.width >= d_body.height) || (alternative)){
        height = d_body.height - 180;
        width = height * (312/448);
    }
    return {width, height}
}

export const PostcardMenuItemView = ({document_body, size_envelope, item, edit_text, state=0}) => {
    const min = (a, b) => {
        return a > b ? b : a;
    }
    // View Main Screen PostcardMenuItem
    const {width, height} = PostcardMenuItemView_dim(document_body);
    const scale_postcard = (state >= 3) ? 1.00 : min((size_envelope.height-10) / height, (size_envelope.width-3) / width);
    const style = {
        height: height,
        width: width,
        backgroundColor: item.color,
        backgroundImage: "url(" + item.source + ")",
        backgroundPosition: "right bottom",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
    }
    const width_text_block = width - 12 - 12;
    return (
        <div className={"postcard-menu envelope-postcard absolute-center"}>
            <div style={{
                transition: "1s",
                transform: "scale("+scale_postcard+")",
                zIndex: "inherit",
            }}>
                <div style={style} className={"postcard-menu-item " + (state >= 2 ? "postcard-anima-1 " : "")}>
                    <div style={{
                        width: width_text_block,
                        boxSizing: "border-box",
                    }} className={"text-block "  + (state >= 2 ? "postcard-anima-1-text " : "")}>
                        <span className="text">{edit_text}</span>
                    </div>
                </div>
            </div>
        </div>
    );
}

function envelope_dim(d_body){
    let alternative = false;
    let width = 0;
    let height = 0;
    if (d_body.width < d_body.height){
        width = d_body.width - 32 - 32;
        height = width * (364/296);
        alternative = (height > (d_body.height - 180));
    }
    if ((d_body.width >= d_body.height) || (alternative)){
        height = d_body.height - 180;
        width = height * (296/364);
    }
    return {width, height}
}

export const Envelope = ({from_user, to_user=null, mark, is_active=true, design_item=null, postcard_text="", setStart=null, setFinish=null}) => {
    const document_body = useWindowDimensions();
    const [DimensionsEnvelope, setDimensionsEnvelope] = useState(envelope_dim(document_body));

    useEffect(() => {
        setDimensionsEnvelope(envelope_dim(document_body));
    }, [document_body]);

    const [state, setState] = useState(0);
    const start_anima = () => {
        if (is_active && state === 0){
            if (setStart !== null){
                setStart(true);
            }
            setState(1);
            setTimeout(() => {
                setState(2);
                setTimeout(() => {
                    setState(3);
                    setTimeout(() => {
                        if (setFinish != null){
                            setFinish(true);
                        }
                    }, 1200);
                }, 2800);
            }, 500);
        }
    }
    return (
        <div style={{
            width: DimensionsEnvelope.width,
            height: DimensionsEnvelope.height
        }} className={"envelope__container " + ((state >= 1 && state < 3)? "perspective " : "") }>
            <div onClick={() => {
                start_anima();
            }} style={{
                width: DimensionsEnvelope.width,
                height: DimensionsEnvelope.height
            }} className={"envelope_move " + (state >= 2 ? "postcard-anima-2 " : "")}>
                <div style={{
                    width: DimensionsEnvelope.width,
                    height: 68/296 * DimensionsEnvelope.width,
                }} className={"envelope__top " + (state >= 1 ? "open " : "")}>
                    <img src={topEnvelope} alt="topEnvelope"/>
                </div>
                <div className={"envelope__base"}>
                    <div className="envelope__base__header">
                    <span className="first_text">
                        Карточка почтовая
                    </span>
                    </div>
                    <div className={"envelope__base__title"}>
                        {from_user !== null &&
                            <NameInEnvelopeAndAvatar user={from_user} text={"От"}/>
                        }
                        {to_user !== null &&
                            <NameInEnvelopeAndAvatar user={to_user} text={"Кому"}/>
                        }
                        <div/>
                        <EnvelopeElementWriteIndex/>
                    </div>
                    <div style={{
                        position: "absolute",
                        transform: "matrix(1, 0.05, -0.05, 1, 0, 0)",
                        width: DimensionsEnvelope.width * 0.245,
                        height: (mark.height / mark.width) * DimensionsEnvelope.width * 0.245,
                        right: "15px",
                        bottom: "19px"
                    }} className={"mark_item"}>
                        <img style={{
                            width: "inherit",
                            height: "inherit"
                        }} src={mark.source} alt="MarkItem"/>
                    </div>
                </div>
            </div>
            {is_active &&
            <PostcardMenuItemView document_body={document_body} item={design_item}
                                  edit_text={postcard_text} size_envelope={
                                      {
                                          width: DimensionsEnvelope.width,
                                          height: DimensionsEnvelope.height
                                      }
                                  }
                                  state={state}
            />
            }
        </div>
    )
}
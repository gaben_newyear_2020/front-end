import React from "react";

const MarkItem = ({mark, max_height}) => {
    let styles = {
        height: max_height,
        width: (mark.width / mark.height) * max_height
    }
    return (
        <div style={{...styles}} className="MarkItem--view">
            <img src={mark.source} alt="MarkItem"/>
        </div>
    );
}
export default MarkItem;
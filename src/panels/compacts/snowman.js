import React from "react";
import {ReactComponent as ReactLogo} from './snowman.svg';
const Snowman = () => {
    return (
        <div className={"Logo"}>
            <ReactLogo/>
        </div>
    );
}
export default Snowman;


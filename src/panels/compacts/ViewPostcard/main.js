import React, {useEffect, useState} from "react";
import "./style.css";
import useWindowDimensions from "../../../methods/hooks/useWindowDimensions";
import MarkItem from "../markItem";
import {Div} from "@vkontakte/vkui";
import {Link} from "react-router-dom";

const save_vk_object = (object) => {
    /* Конвертер ВК объекта пользователя в строку параметра url */
    localStorage.setItem("postcard_answer_object", JSON.stringify(object));
}

const ViewPanelPostcard = ({Width, backplace, hookLoad, mark, PostcardItem, isAnon, UserFetch, FromUser, user_text}) => {
    const height = Width * 96 / 183;
    return (
        <Div>
            <div className={"postcard-view-panel " + (backplace ? "backplace " : "")}>
                <div style={{minHeight: height}} className="postcard-view-panel__layout front">
                    <div className={"postcard-view-panel__text " + PostcardItem.classItem}>
                        {user_text}
                    </div>
                    <img src={PostcardItem.source} alt={"ViewImage"} onLoad={hookLoad}/>
                </div>
                <div style={{minHeight: height}} className="postcard-view-panel__layout back">
                    <div className="postcard-view-back">
                        <div className={"postcard-view-panel-back__text first"}>
                            <strong>Отправитель:</strong><br/> {isAnon? "Аноним" : FromUser.first_name + " " + FromUser.last_name}
                        </div>
                        <div className={"postcard-view-panel-back__text second"}>
                            <span>
                                <strong>Получатель:</strong><br/> {UserFetch.first_name} {UserFetch.last_name}
                            </span>
                        </div>
                        <div className="mark-index">
                            <MarkItem mark={mark} max_height={height * 0.38}/>
                        </div>
                    </div>
                </div>
            </div>
        </Div>
    )
}

const ViewPostcardImage = ({dataLoad, Mark, Postcard, fetchedUser}) => {
    const isAnon = dataLoad.from_user === null;
    if (!isAnon){
        save_vk_object(dataLoad.from_user);
    }
    const { height, width } = useWindowDimensions();
    const width_element = (width - 32) > 479 ? 479 : (width - 32);
    const height_element = Math.round((273.703 / 479) * width_element);
    // Use Constant project
    const start_element_top_text = ((height - height_element) / 2) - 16;
    const start_element_top_bottom = ((height + height_element) / 2);

    const [state, setState] = useState(0);
    const [load, setLoad] = useState(false);
    const [loadPreHook, setLoadPreHook] = useState(false);
    const [loadImage, setLoadImage] = useState(0);
    const [flipPostcard, setFlipPostcard] = useState(false);
    const url = process.env.PUBLIC_URL + "/postcards/ViewPostcardImage/";
    const first_envelop_start = useState(true);

    useEffect(() => {
        if (loadImage === 4){
            setLoadPreHook(true);
            setTimeout(() => {
                setLoad(true);
            }, 1500);
        }
    }, [loadImage])

    function addCompleteImage(){
        setLoadImage(loadImage + 1);
    }
    function envelopStart(){
        if (load && first_envelop_start[0]){
            first_envelop_start[1](false);
            setState(1);
            setTimeout(() => {setState(2);}, 430);
            setTimeout(() => {setState(3);}, 5430);
            setTimeout(() => {setState(4);}, 5830);
        }
    }
    return (
        <div className={"PostcardView"}>
            <div hidden={state !== 0} style={{top: start_element_top_text}} className={"PostcardView__text"}>
                Для вас приготовили поздравление! <br />
                Откройте конверт и взгляните на него!
            </div>
            <div hidden={!loadPreHook} className={"section"} onMouseDown={() => {return false}} onSelectStart={() => {return false}}>
                <div onClick={envelopStart} style={state >= 2 ? {transform: "translateY(20%) scale(1)", width: width_element, height: height_element} : {width: width_element, height: height_element}} className={"envelope " + (load? "" : "envelope_ready ") + (state >= 1? "env_scale ": "") + (state >= 4? "env_clear ": "")}>
                    <div className={"envelope_top " + ((state >= 1) && (state < 3)? "envelope_open " : "")}>
                        <img onLoad={addCompleteImage} src={url + "env_top.svg"} alt="TopLayout" />
                    </div>
                    <div className={"envelope_back " + ((state >= 1) && (state < 3)? "envelope_back_view " : "")}>
                        <img onLoad={addCompleteImage} src={url + "env_back.svg"} alt="BackLayout" />
                    </div>
                    <div onClick={() => {
                        if (state >= 3){
                            setFlipPostcard(!flipPostcard);
                        }
                    }} style={{width: 0.9*width_element, opacity: state === 0? 0 : 1}} className={"paper " + (state >= 1? "paper_top_effect " : " ") + (state >= 2? "paper_start " : " ")}>
                        <ViewPanelPostcard hookLoad={addCompleteImage}
                                   FromUser={isAnon ?
                                       {first_name: "Анон", last_name: ""} :
                                       {first_name: dataLoad.from_user.first_name, last_name: dataLoad.from_user.last_name}
                                   }
                                   FromUserId = {isAnon ? null : dataLoad.from_user.id}
                                   UserFetch={{first_name: fetchedUser.first_name, last_name: fetchedUser.last_name}}
                                   isAnon={isAnon}
                                   user_text={dataLoad.text}
                                   mark={Mark}
                                   PostcardItem={Postcard}
                                   Width={0.9*width_element}
                                   backplace={flipPostcard}
                        />
                    </div>
                    <div className={"envelope_body "}>
                        <img onLoad={addCompleteImage} src={url + "env_body.svg"} alt="BodyLayout" />
                    </div>
                </div>
            </div>
            {
                isAnon && (
                    <Link hidden={state < 4} style={{top: start_element_top_bottom, width: width_element}} className={"PostcardView__btn"} to="/postcard-new/">
                        Сделать свою открытку
                    </Link>
                )
            }
            {
                !isAnon &&
                (
                    <Link hidden={state < 4} style={{top: start_element_top_bottom, width: width_element}} className={"PostcardView__btn"} to={"/postcard-new/answer"}>
                        Отправить в ответ
                    </Link>
                )
            }
        </div>
    )
}

export default ViewPostcardImage;
import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
import {Spinner, Panel, Tabbar} from '@vkontakte/vkui';
import * as api from "../../../methods/api";
import topEnvelope from "../icons/topEnvelope.svg";
import indexDigitPostcard from "../icons/indexDigitPostcard.png";
import {ButtonUiOrange, Envelope, PanelHeaderUi, PrevArrowIcon, TabbarUi} from "../components";

function getMarkById(uuid){
    let data = api.allPostcardMarkItems();
    uuid = uuid % data.length;
    return data[uuid];
}
function getPostcardById(uuid){
    let data = api.allPostcardShowItems();
    for (let i = 0; i < data.length; i++){
        if (data[i].id === uuid){
            return data[i];
        }
    }
    return null;
}
function link_to_click() {
    try{
        document.querySelector("#postcard-view-back").click();
    } catch (e) {}
}

let styles_spinner = {
    margin: "auto", top: "50%", left: "50%", transform: "translate(-50%, -50%)",
    position: "fixed"
}

const PostcardView = ({id, back, appUser, fetchedUser, slug, MainScreenCounterUnreadState}) => {
    const [data, setData] = useState({
        from_user_id: -1,
        from_user: {
            first_name: "",
            last_name: "",
        },
        text: "",
        design: {
            id: 1,
            url: "/postcard.svg"
        },
        slug: "",
        is_view: false,
        time: null,
        mark_id: 0,
    });
    const [Postcard, setPostcard] = useState(getPostcardById(data.design.id));
    const [Mark, setMark] = useState(getMarkById(data.mark_id));

    const [loader, setLoader] = useState( <Spinner size="large" style={styles_spinner} />);
    const [loaderState, setLoaderState] = useState( 0);
    const [isOpenEnvelope, setOpenEnveloper] = useState( false);
    const [isFinishEnveloper, setFinishEnveloper] = useState( false);

    useEffect(() => {
        async function fetchedData(){
            if (slug.length <= 0){
                return link_to_click();
            }
            let data = await api.postcard_get(appUser.sign, slug);
            console.log(data);
            if (data === null){
                return link_to_click();
            }
            let design = getPostcardById(data.design.id);
            if (design === null){
                return link_to_click();
            }
            setPostcard(design);
            setMark(getMarkById(data.mark_id));
            setData(data);
            setLoaderState(1);
            if (data.is_view === false){
                MainScreenCounterUnreadState[1](MainScreenCounterUnreadState[0] - 1);
            }
        }
        fetchedData();
    }, []);
    useEffect(() => {
        if (loaderState >= 5){
            setLoader(null);
        }
    }, [loaderState]);
    return (
        <Panel id={id}>
            <div style={{position: "relative", height: "100vh"}} className={"blue_background_gradient"}>
                <PanelHeaderUi id={"headerFirstScreen"} icon={
                    <Link to={"/postcard/"}><PrevArrowIcon /></Link>
                }>Подарок</PanelHeaderUi>
                {loader}
                {loader !== null && (loaderState >= 1 && loaderState < 5) &&
                    <div style={{display: "none"}}>
                        <img onLoad={() => {setLoaderState(loaderState + 1)}} src={Postcard.source} alt="PostcardLoader"/>
                        <img onLoad={() => {setLoaderState(loaderState + 1)}} src={Mark.source} alt="PostcardLoader"/>
                        <img onLoad={() => {setLoaderState(loaderState + 1)}} src={topEnvelope} alt="PostcardLoader"/>
                        <img onLoad={() => {setLoaderState(loaderState + 1)}} src={indexDigitPostcard} alt="PostcardLoader"/>
                    </div>
                }
                {loader === null && (
                    <div style={{transition: ".3s"}} className={"absolute-center"}>
                        {!isOpenEnvelope &&
                            <p className={"postcard-top-open-text"}>Наши почтовые гномы доставили вам письмо. Откройте конверт, чтобы прочитать <span role="img">🎁</span><span role="img">🎄</span><span role="img">🎅🏻</span></p>
                        }
                        <div className={"postcard-three-panel"}>
                            <Envelope is_active={true} from_user={data.from_user} to_user={fetchedUser} mark={Mark}
                                      postcard_text={data.text}
                                      design_item={Postcard}
                                      setStart={setOpenEnveloper}
                                      setFinish={setFinishEnveloper}
                            />
                        </div>
                    </div>
                )}
                <TabbarUi>
                    <div className={"button-down " + (isFinishEnveloper ? "" : "hide")}>
                        <Link id={"postcard-view-back"} to={"/"}>
                            <ButtonUiOrange addIcon={true}>Отправить в ответ</ButtonUiOrange>
                        </Link>
                    </div>
                </TabbarUi>
            </div>
        </Panel>
    )
};

export default PostcardView;

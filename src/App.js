import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Link} from 'react-router-dom';
import bridge from '@vkontakte/vk-bridge';
import View from '@vkontakte/vkui/dist/components/View/View';
import ScreenSpinner from '@vkontakte/vkui/dist/components/ScreenSpinner/ScreenSpinner';
import '@vkontakte/vkui/dist/vkui.css';
import "./fonts/ptastraserif.css";
import "./App.css";

import * as api from "./methods/api";
import PostcardAll from "./panels/PostcardAll";
import PostcardNew from "./panels/PostcardNew/PostcardNew";
import PostcardView from "./panels/compacts/ViewPostcard/PosctardView";
import MainScreen from "./panels/MainScreen";
import EditTextScreen from "./panels/EditText";

function link_to_click() {
	document.querySelector("#link-to").click();
}

const App = () => {

	const [fetchedUser, setUser] = useState(null);
	const [appUser, setAppUser] = useState(null);
	const [linkTo, setLinkTo] = useState("/");
	const [popout, setPopout] = useState(<div id="spinner"><ScreenSpinner size='large' /></div>);
	const MainScreenCounterUnreadState = useState(0);

	useEffect(() => {
		bridge.send("VKWebAppInit");
		bridge.subscribe(({ detail: { type, data }}) => {
			if (type === 'VKWebAppUpdateConfig') {
				const schemeAttribute = document.createAttribute('scheme');
				let scheme = data.scheme ? data.scheme : 'client_light';
				if ((data.appearance !== undefined) && (data.appearance !== null)){
					scheme = (data.appearance === "light") ? 'client_light' : 'space_gray';
				}
				schemeAttribute.value = scheme
				document.body.attributes.setNamedItem(schemeAttribute);
			}
		});
		async function fetchData() {
			const sign = window.location.href;
			const params_array = window.location.href.split("#");
			let params = new URLSearchParams();
			if (params_array.length >= 1){
				params = new URLSearchParams(params_array[1]);
			}

			// Get Info VK
			const user = await bridge.send('VKWebAppGetUserInfo');
			setUser(user);

			// Get Info startApp server
			let app_user = await api.startApp(sign);
			app_user.sign = sign;
			setAppUser(app_user);
			//console.log(app_user);
			MainScreenCounterUnreadState[1](app_user.unread);

			if ((params.get("link") !== undefined) && (params.get("link") !== null)){
				setLinkTo("/" + params.get("link"));
				link_to_click();
			}
			setPopout(null);
		}
		fetchData();
	}, []);

	const Wrapper = (id, component) => (
		<View activePanel={id}>{component}</View>
	);

	const RouteHome = () => Wrapper('home', <MainScreen id="home" MainScreenCounterUnread={MainScreenCounterUnreadState[0]} appUser={appUser}/>);
	const RouteEditTextScreen = () => Wrapper('edit', <EditTextScreen id="edit" fetchedUser={fetchedUser} appUser={appUser}/>);
	const RoutePostcardView = (props) => Wrapper('postcard_view', <PostcardView MainScreenCounterUnreadState={MainScreenCounterUnreadState} slug={props.match.params.slug} fetchedUser={fetchedUser} appUser={appUser} back="/postcard/" id="postcard_view"/>);
	const RoutePostcardAll = () => Wrapper('postcard_all', <PostcardAll id="postcard_all" fetchedUser={fetchedUser} appUser={appUser}/>);
	const RoutePostcardNew = (props) => Wrapper('postcard_new', <PostcardNew isAnswer={props.match.params.item === "answer"} id="postcard_new" back="/" fetchedUser={fetchedUser} appUser={appUser}/>);

	return (
		<Router>
			{popout}
			<Link id="link-to" to={linkTo}/>
			<div>
				<Route path="/" exact component={RouteHome} />
				<Route path="/edit/" exact component={RouteEditTextScreen} />
				<Route path="/postcard/" exact component={RoutePostcardAll} />
				<Route path="/postcard-new/:item?" component={RoutePostcardNew} />
				<Route path="/postcard/:slug/" component={RoutePostcardView} />
			</div>
		</Router>
	);
}

export default App;

